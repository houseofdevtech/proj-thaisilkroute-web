target_redirect = "";

//$(document ).ready(function() {

   $("#radio2").attr('checked', 'checked');
   $("#radiox2").attr('checked', 'checked');

   pathname = window.location.pathname;
   pathname = pathname.split("/");
   filename = pathname[pathname.length-1].split(".html");
   language = filename[0].split("_");
   language = language[language.length-1];

   if(filename[0] == "index_eng"){
      $("#radio1").attr('checked', 'checked');
      $("#radiox1").attr('checked', 'checked');
      target_redirect = "index.html";
   }
   else if(language == "eng"){
      $("#radio1").attr('checked', 'checked');
      $("#radiox1").attr('checked', 'checked');
      target_redirect = pathname[pathname.length-1].replace(/_eng/g, "_th");
   }
   else if(language == "th"){
      $("#radio2").attr('checked', 'checked');
      $("#radiox2").attr('checked', 'checked');
      //alert(pathname[pathname.length-1]);
      target_redirect = pathname[pathname.length-1].replace(/_th/g, "_eng");
   }
   else if(filename[0] == "index" || filename[0] == ""){
      $("#radio2").attr('checked', 'checked');
      $("#radiox2").attr('checked', 'checked');
      target_redirect = "index_eng.html";
   }
   
   //alert(target_redirect);

   var $radios = $('input[type="radio"][name="switchone"]')
   $('.switch-fieldp').click(function() {
      var $checked = $radios.filter(':checked');
      var $next = $radios.eq($radios.index($checked) + 1);
      if(!$next.length){
         $next = $radios.first();
      }
      $next.prop("checked", true);

      window.location.href = target_redirect;
   });

   var $radiosx = $('input[type="radio"][name="lang-one"]')
   $('.switch-fieldx').click(function() {
      var $checked = $radiosx.filter(':checked');
      var $next = $radiosx.eq($radiosx.index($checked) + 1);
      if(!$next.length){
         $next = $radiosx.first();
      }
      $next.prop("checked", true);

      window.location.href = target_redirect;
      //alert(target_redirect);

   });

//});
