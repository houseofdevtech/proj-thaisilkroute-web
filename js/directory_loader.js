var object_data;
var language;
var shop_province_element_count = [];
var travel_province_element_count = [];
var all_province_mapping = [];
all_province_mapping["center"] = ["Bangkok", "Nonthaburi", "Pathum Thani", "Suphan Buri", "Ratchaburi", "Chai Nat", "Chon Buri"];
all_province_mapping["northeast"] = ["Kalasin", "Khon Kaen", "Chaiyaphum", "Nakhon Phanom", "Nakhon Ratchasima", "Bueng Kan", "Buriram", "Maha Sarakham", "Yasothon", "Roi Et", "Loei", "Si Sa Ket", "Sakon Nakhon", "Surin", "Nong Khai", "Nong Bua Lamphu", "Udon Thani", "Ubon Ratchathani"];
all_province_mapping["south"] = ["Surat Thani"];
all_province_mapping["north"] = ["Kampaengpetch", "Chiang Rai", "Chiang Mai", "Nakhon Sawan", "Phichit", "Phrae", "Lamphun", "Nan", "Sukhothai", "Uttaradit", "Uthai Thani", "Tak"];


$(document).ready(function () {
	$(document).on("click", ".tab", function () {
		mapFuc2($(this).attr("id").split('tab')[1]);
	});
	$(document).on("click", ".mapx", function () {
		mapFuc2($(this).attr("id"));
	});
	$(function () {
		$(document).on("click", ".shox", function () {
			//$(" .col-lg-4").show(2000);
			$("#sub" + $(this).attr("province_id") + " > .row").find(".col-lg-4").show('slow');
			$(this).hide();
		});
		$(document).on("click", ".vox", function () {
			//$(" .col-lg-4").show(2000);
			$("#tsub" + $(this).attr("province_id") + " > .row").find(".col-lg-4").show('slow');
			$(this).hide();
		});
	});
});


$(document).ready(function () {

	url = $(location).attr('href');
	pathname = window.location.pathname;
	pathname = pathname.split("/");
	filename = pathname[pathname.length - 1].split(".html")[0];
	filename_seperate = filename.split("_");
	language = filename_seperate[filename_seperate.length - 1];
	filename_without_lang = filename.split("_" + language)[0];
	contact_wording = "ติดต่อ";
	shop_wording = "ดูร้านค้าอื่น ๆ เพิ่มเติม";
	travel_wording = "ดูสถานที่ท่องเที่ยวอื่น ๆ เพิ่มเติม";
	read_more_wording = "อ่านต่อ";
	eng_suffix = "";

	region = "";
	if (filename_seperate[1] == "center") {
		region = "Central Region";
	}
	else if (filename_seperate[1] == "northeast") {
		region = "North Eastern";
	}
	else if (filename_seperate[1] == "south") {
		region = "Southern Region";
	}
	else if (filename_seperate[1] == "north") {
		region = "North";
	}
	if (language == "eng") {
		contact_wording = "Contacts";
		shop_wording = "See more shops";
		travel_wording = "See more travel destinations";
		read_more_wording = "Read More"
		eng_suffix = "_eng";
	}

	var province_mapping = all_province_mapping[filename_seperate[1]];

	$("div").on("click", ".read_more", function () {
		$(this).hide();
		index = $(this).attr("shop_index");
		$(".shop_info_" + index).css("height", "auto");
		//console.log($(".shop_area_"+index));
		$(".shop_area_" + index).attr("style", "height: auto !important");
		$(this).find('.read_more').css("height", "auto");
	});

	$.each(province_mapping, function (key, value) {
		shop_province_element_count[value] = 0;
		travel_province_element_count[value] = 0;
		province_template = `
			<div id="sub0`+ (key + 1) + `" province_mapping="` + value + `" class="province_level carousel slide col none-xl m " data-ride="carousel" data-interval="90000">
            <div class="carousel-inner row " role="listbox">
               <!--area for card-->
            </div>
            <a class="carousel-control-prev pc-x" href="#sub0`+ (key + 1) + `" role="button" data-slide="prev">
               <img src="images/_l.png">
            </a>
            <a class="carousel-control-next pc-x text-faded" href="#sub0`+ (key + 1) + `" role="button" data-slide="next">
               <img src="images/_r.png">
            </a>
            <div class="col text-center pc-x num-dov" style="display:none;">
               1 / 9
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col text-right none-pad mb-x">
               <a href="javascript:;" provinceeng="`+ value + `" province_id="0` + (key + 1) + `" class="shox" style="display:none;">` + shop_wording + `</a>
            </div>   
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
     	</div>
    `;
		$('.before-region-area').after(province_template);

		travel_template = `
	    <div id="tsub0`+ (key + 1) + `" tprovince_mapping="` + value + `" class="province_level carousel slide col none-xl m " data-ride="carousel" data-interval="90000">
            <div class="carousel-inner row " role="listbox">
              <!--card area-->                     
            </div>

            <div class="col text-center pc-x num-dov" style="display:none;">
               1 / 9
            </div>
            <a class="carousel-control-prev pc-x" href="#tsub0`+ (key + 1) + `" role="button" data-slide="prev">
               <img src="images/_l.png">
            </a>
            <a class="carousel-control-next pc-x text-faded" href="#tsub0`+ (key + 1) + `" role="button" data-slide="next">
               <img src="images/_r.png">
            </a>
            <div class="clearfix">&nbsp;</div>
            <div class="col text-right none-pad mb-x">
               <a href="javascript:;" provinceeng="`+ value + `" province_id="0` + (key + 1) + `" class="vox" style="display:none;">` + travel_wording + `</a>
            </div>   
            <div class="clearfix">&nbsp;</div>
            <div class="clearfix">&nbsp;</div>
         </div>
    `;
		$('.before-travel-area').after(travel_template);
	});
	$(".province_level").hide();
	$("#sub01").show();
	$("#tsub01").show();


	$.ajax({
		url: "../directory_data/shop/shop-2020-feb-26.txt?" + Date.now(), success: function (data) {
			obj_data = JSON.parse(data);
			//console.log(obj_data);
			$.each(obj_data, function (index, value) {
				if ($.trim(value.regioneng) == region) {
					carousel_active = "";
					if (shop_province_element_count[$.trim(value.provinceeng)] == 0) {
						carousel_active = " active ";
					}
					shop_province_element_count[$.trim(value.provinceeng)]++;
					card_template = `<div class="col-lg-4 mr-top carousel-item ` + carousel_active + `">
				   <div class="col none-pad img-div2 text-center">
				      <div style="display:none;">`+ eval('value.shop' + language) + `</div>
				      <img onerror="this.src='directory_data/shop/shop_image/`+ value.thumbnailimagename + `';" src="directory_data/shop/shop_image/` + value.thumbnailimagename.split(".")[0].split("_")[1] + "_" + value.thumbnailimagename.split("_")[0] + eng_suffix + "." + value.thumbnailimagename.split(".")[1] + `" class="img-fluid d-block mx-auto">
				   </div>
				   <div class="col text-div shop_area_`+ index + `" style="height:500px !important;">
				      <h3>`+ eval('value.shop' + language) + `</h3>
				      <p class="shop_info_`+ index + `" style="height: 5.3em;display:block;overflow:hidden;">` + eval('value.info' + language) + `</p>
				      <div class="col text-right" style="padding:0;">
				         <!--<a href="javascript:;" data-toggle="modal" data-target="#artModel" style="">อ่านเพิ่มเติม</a>-->
				         <a shop_index="`+ index + `" class="read_more" style="">` + read_more_wording + `</a>
				      </div>
				      <div class="col none-pad bot-div">
				         <h6>` + contact_wording + `</h6>
				         <a href="tel:`+ value.phone + `"><img src="images/tel_c.png"> ` + value.phone + ` </a>
				         <br>
				         <a href="javascript:;"><img src="images/line_c.png"> `+ value.lineid + `</a>
				         <br>
				         <a href="javascript:;"><img src="images/fa_c.png"> `+ value.facebook + ` </a>
				         <p style="height:5em;">`+ eval('value.address' + language) + ` ` +
						eval('value.tambon' + language) + ` ` + eval('value.amphor' + language) + ` ` +
						eval('value.province' + language) + ` 
				         </p>
				      </div>
				   </div>
				   <div class="clearfix pc-x">&nbsp;</div>
				   <div class="clearfix pc-x">&nbsp;</div>
				</div>`;

					$("[province_mapping='" + $.trim(value.provinceeng) + "'] > .carousel-inner").append(card_template);
				}
			});

			$.each(province_mapping, function (key, value) {
				if (shop_province_element_count[$.trim(value)] > 3) {
					// alert($.trim(value));
					$('.shox[provinceeng="' + $.trim(value) + '"]').show();
				}
				set_slide_mobile(key);
			});
		}
	});

	$.ajax({
		url: "../directory_data/travel/travel-2020-jan-13.txt?" + Date.now(), success: function (data) {
			obj_data = JSON.parse(data);
			//console.log(obj_data);
			$.each(obj_data, function (index, value) {
				if ($.trim(value.regioneng) == region) {
					carousel_active = "";
					if (travel_province_element_count[$.trim(value.provinceeng)] == 0) {
						carousel_active = " active ";
					}
					travel_province_element_count[$.trim(value.provinceeng)]++;
					card_template = `
   					<div class="col-lg-4 carousel-item mr-top `+ carousel_active + `">
   		                  <div class="col img-div text-right" >
   		                     <img src="directory_data/travel/travel_image/`+ value.imagename + `" class="img-fluid d-block mx-auto">
   		                  </div>
   		                  <div class="col text-div " style="height:500px !important;">
   		                     <h3>`+ eval('value.place' + language) + `</h3>
   		                     <p>`+ eval('value.detail' + language) + `</p>
   		                  </div>
   		                  <div class="clearfix pc-x">&nbsp;</div>
                  	 	</div>          
   				`;
					// console.log(card_template);
					// console.log("[tprovince_mapping='"+$.trim(value.provinceeng)+"'] > .carousel-inner");
					$("[tprovince_mapping='" + $.trim(value.provinceeng) + "'] > .carousel-inner").append(card_template);
				}
			});

			$.each(province_mapping, function (key, value) {
				if (shop_province_element_count[$.trim(value)] > 3) {
					$('.vox[provinceeng="' + $.trim(value) + '"]').show();
				}
				set_slide_mobile(key);
			});
		}
	});

});

function set_slide_mobile(key) {
	var width = $(window).width();
	if (width > 992) {
		$(".carousel-inner .carousel-item").removeClass("carousel-item");
	}

	$(document).ready(function () {
		var numItems = $('.carousel-item').length;
		$('.num-dov').html("1 / " + numItems);
	});

	$("#sub0" + (key + 1) + " .carousel-control-prev").on('click', function (event) {
		var numItems = $('.carousel-item').length;
		var intf = parseFloat($("#sub01 .num-dov").text().charAt(0));
		if (intf == 1) {
			intf = numItems;
			var intx = (numItems);
		}
		else {
			var intx = (intf - 1);
		}
		$('#sub0' + (key + 1) + ' .num-dov').html(intx + "  / " + numItems);
	});

	$("#sub0" + (key + 1) + " .carousel-control-next").on('click', function (event) {
		var numItems = $('.carousel-item').length;
		var intf = parseFloat($("#sub0" + (key + 1) + " .num-dov").text().charAt(0));
		if (intf == numItems) {
			intf = 0;
		}
		var intx = (intf + 1);
		$('#sub0' + (key + 1) + ' .num-dov').html(intx + "  / " + numItems);
	});

	jQuery("#sub01.carousel").swipe({
		swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
			if (direction == 'left') {
				var numItems = $('.carousel-item').length;
				var intf = parseFloat($("#sub01 .num-dov").text().charAt(0));
				if (intf == numItems) {
					intf = 0;
				}
				var intx = (intf + 1);
				$('#sub01 .num-dov').html(intx + "  / " + numItems);
				$(this).carousel('prev');
			}
			if (direction == 'right') {
				var numItems = $('.carousel-item').length;
				var intf = parseFloat($("#sub01 .num-dov").text().charAt(0));
				if (intf == 1) {
					intf = numItems;
					var intx = (numItems);
				}
				else {
					var intx = (intf - 1);
				}
				$('#sub01 .num-dov').html(intx + "  / " + numItems);
				$(this).carousel('next');

			}
		},
		allowPageScroll: "vertical"
	});






	// var width = $(window).width();
	//      if (width>992) {
	//         $(".carousel-inner .carousel-item").removeClass("carousel-item");
	//      }
	//      $(document).ready(function() {
	//         var numItems = $('.carousel-item').length;      
	//         $('.num-dov').html("1 / "+numItems);
	//      });

	//      $(document).on('click',".carousel-control-prev", function(event){   
	//         var numItems = $('.carousel-item').length; 
	//         var intf = parseFloat($(".num-dov").text().charAt(0));
	//         if (intf==1){
	//            intf=numItems;
	//            var intx = (numItems);
	//         } 
	//         else{
	//            var intx = (intf-1);
	//         }

	//         $('.num-dov').html(intx+"  / "+numItems);
	//      });
	//      $(document).on('click', ".carousel-control-next", function(event){   
	//         var numItems = $('.carousel-item').length; 
	//         var intf = parseFloat($(".num-dov").text().charAt(0));
	//         if (intf==numItems){
	//            intf=0;
	//         } 
	//         var intx = (intf+1);
	//         $('.num-dov').html(intx+"  / "+numItems);
	//      });
}

function mapFuc2(id) {
	var mapId = document.getElementById(id);
	var mapId2 = document.getElementById("tab" + id);

	$(".mapx").removeClass("active");
	$(".ul li div").removeClass("active");
	//$(".main1").hide();
	//$("#data"+id).show(); 
	mapId.classList.add("active");
	mapId2.classList.add("active");

	$(".province_level").hide();
	$("#sub" + id + "").show();
	$("#tsub" + id + "").show();

}
function mapFuc(id) {


}
